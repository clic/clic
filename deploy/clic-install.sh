#!/bin/bash

# set -e ?

apt update && apt full-upgrade -y

systemctl disable systemd-resolved
systemctl stop systemd-resolved

echo 'nameserver  80.67.169.12' > /etc/resolv.conf
echo 'nameserver  80.67.169.40' >> /etc/resolv.conf
echo 'nameserver  2001:910:800::12' >> /etc/resolv.conf
echo 'nameserver  2001:910:800::40' >> /etc/resolv.conf

apt install git curl wget -y
curl https://install.yunohost.org > /tmp/install-yunohost.sh
chmod +x /tmp/install-yunohost.sh
/tmp/install-yunohost.sh -a

git clone https://framagit.org/clic/clic /var/www/install_clic
cd /var/www/install_clic
source deploy/deploy.sh
