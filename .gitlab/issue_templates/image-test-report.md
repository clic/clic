_To use this template, read [the image testing notice](https://framagit.org/clic/clic/-/wikis/Testing-Images-for-ARM-boards)_  

## Test procedure

1. `[]` download the image you want to test and its checksum file from https://image-builds.yunohost.org/  
1. `[]` verify the downloaded files : `shasum -c clic_version_arch-date.img.gz.sha256`  
1. `[]` decompress the image : `gunzip clic_version_arch-date.img.gz`  
1. `[]` write the image to a a boot media with dd : `dd if=clic_version_arch-date.img of=/dev/sdx bs=4M status=progress conv=fsync oflag=direct`  
1. `[]` boot the device with the imaged media  
1. `[]` test if the device ip address can be found with mdns : `ping clic.local` (note : if you have another device on your local area network that is already broadcasting the clic.local name, your test device may have decided to use a different name, like clic-2.local)  
1. `[]` connect to the device with ssh : ssh root@clic.local (notes : root password is `yunohost`, you may need to remove any ssh fingerprint left by a previous test with `ssh-keygen -R clic.local`)  
1. `[]` check that the device can resolve dns names : `ping yunohost.org`  
1. `[]` check that the wifi interface is detected (some boards do not have integrated wifi and need to plug an usb wifi dongle. and that dongle may need a driver to be installed)  
1. `[]` from your workstation, open the post install in your web browser : https://clic.local  
1. `[]` fill all the fields on the post install form. For the main domain put a sub domain of one of the yunohost dyndns domain (.nohost.me, .noho.st or .ynh.fr). Be carefull about the user password you type. You will need to put the same password if you want to re-use the same dyndns domain on another test.  
1. `[]` enable all the applications  
1. `[]` clic the `Let's go!` button  
1. `[]` enjoy a nice coffee while you wait for the post-install completion  
1. `[]` when the `Installation complete` page is displayed, clic on the `Go to the portal` which should send you to the web admin login page  
1. `[]` check that you can login with the user and password you entered on the post-install form  
1. `[]` connect your workstation, or another device, to the `ClicHotspot` WiFi (passphrase is also `ClicHotspot`) and make sure it is the only active network interface (if using a mobile phone, disable mobile data)  
1. `[]` On the web admin page, clic on the `User interface` button which should send you to the user portal login page  
1. `[]` check that you can login with the user and password you entered on the post-install form  
1. `[]` check that all the application are present on the user portal  
1. `[]` go to the YesWiki application  
    1. `[]` check that the YesWiki landing page has the correct CLIC default content  
    1. `[]` check that you are automatically logged in YesWiki and that your user is admin  
    1. `[]` select the `Services` menu at the top of the YesWiki page and check that all the applications are listed with their logo displayed  
    1. `[]` create a user and test login from yeswiki that redirect to SSO and redirect back to yeswiki
1. `[]` back to the user portal, go to the etherpad application and create a test pad  
1. `[]` go to the hedgedoc app and check that you can login using the yunohost sso method  
1. `[]` go to the nextcloud app  
    1. `[]` check that you are automatically logged-in  
    1. `[]` go to the nextcloud admnistration overview page and check that there are no critical warnings  
1. `[]` Install the wireguard_client application and configure a VPN
1. `[]` Remove the wireguard_client application  
1. `[]` Install the openvpn_client application and configure a VPN
1. `[]` Remove the openvpn_client application  
1. `[]` ...  
1. `[]` ...  
1. `[]` _you can add more tests steps that you have done here_  
1. `[]` ...  
1. `[]` repeat the previous steps using a .local domain  
1. `[]` repeat the previous steps using a real domain, other than the yunohost dyndns domains.  

## Notes
_Test failure details and general comments about your test go here. You can also add comments specific to one test step just below the list item of that step. But in that case make sure you indent all the lines you add with 4 more spaces than the list item to avoid resetting the numbering of the list) and use **bold** formating to make your comment more visible_
