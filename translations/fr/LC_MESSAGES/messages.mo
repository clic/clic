��    +      t              �     �  
   �     �             ?        ^  
   p  ?   {     �  	   �  
   �     �     �  1     ;   F  	   �     �     �     �     �     �  '   �     �  ]     G   z     �  	   �     �     �  $     �   (  V        l     u  6   �     �  5   �  �        �     	      	  s  &	     �
     �
     �
     �
     �
  J   �
  (   I  
   r  C   }     �     �     �     
       /   8  ;   h     �     �  
   �     �     �  &   �  ;     %   [  p   �  S   �  
   F     Q     ^     w  3   �    �  ^   �     2     >  ^   \     �  ;   �  �                  1   (please wait 1 min) .toml file Checkout the portal Choose file Clic installation Could not load this file as toml ... Is it a valid .toml file ? Custom app bundle Debug mode Don't use root of an existing linux user as username, you fool! First user (administrator) Full name Full reset Go to the portal Installation complete ! It looks like domain %(domain)s is not available. It looks like the board is not connected to the internet !? Let's go! Main domain Name (SSID) Password Password (repeat) Passwords do not match Passwords must be at least 8 characters Please specify a valid domain Please specify a valid username with at least three characters (only lowercase, numbers or _) Please wait just a few more seconds while the installation completes... Retry Sasha Doe Server configuration The password is too simple. The username is already a linux user This is the domain that will be used to name and reach your clic box. If you do not already own a domain name, you can pick a free domain ending with .nohost.me, .noho.st or .ynh.fr. It will automatically be configured during the setup. This is where you'll be able to navigate through installed applications and contents ! Username Wifi Hotspot configuration You can specify a custom app bundle with a .toml here. You have a new WiFi ! You must provide a valid .toml file to enable the VPN Your new wifi is called <span style='font-weight:bold; font-family:monospace;'>%(wifi_ssid)s</span> and should now show up on your devices. When you're connected to it, your traffic is automatically encrypted through the VPN tunnel. myNeutralNetwork myserver.nohost.me sasha Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2023-02-13 13:19+0100
PO-Revision-Date: 2023-02-13 13:21+0100
Last-Translator: 
Language: fr
Language-Team: fr <LL@li.org>
Plural-Forms: nplurals=2; plural=(n > 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.9.1
 (patientez une minute) Fichier .toml Aller sur le portail CLIC Choisir un fichier Clic installation Échec du chargement du fichier .toml ... Est-ce un fichier .toml valide ? Sélection personnalisée d'applications Mode debug N'utilisez pas root ou un compte linux déjà existant, peuchère ! Premier utilisateur.ice (admin) Nom complet Réinitialisation intégrale Aller sur le portail Installation terminée ! Le domaine %(domain)s ne semble pas disponible. Il semble que la carte ne soit pas connecté à Internet !? C'est parti ! Domaine principal Nom (SSID) Mot de passe Mot de passe (confirmation) Les mots de passe ne correspondent pas Les mots de passent doivent contenir au moins 8 caractères Merci de spécifier un domaine valide Merci de spécifier un identifiant valide avec au moins trois caractères (uniquement minuscules, chiffres ou _) Merci de patienter juste quelques secondes pendant que l'installation se termine... Réessayer Sasha Dupont Configuration du serveur Mot de passe trop simple. L'utilisateur·ice choisi·e est déjà présent·e Il s'agit du domaine qui sera utilisé pour nommer et accéder à votre CLIC. Si vous ne possédez pas de nom de domaine, vous pouvez utiliser un domaine gratuit se terminant en .nohost.me, .noho.st ou .ynh.fr. Il sera automatiquement configuré pendant l'installation. C'est l'endroit où vous pourrez naviguer entre les applications installées et les contenus ! Identifiant Configuration du Hotspot WiFi Vous pouvez spécifier un fichier .toml avec une sélection personnalisée d'applications ici. Vous avez un nouveau WiFi ! Il faut fournir un fichier .toml valide pour activer le VPN Votre nouveau wifi se nomme <span style='font-weight:bold; font-family:monospace;'>%(wifi_ssid)s</span> et devrait appaître sur vos dispositifs. Lorsque vous êtes connecté dessus, le traffic est automatiquement chiffré à travers le tunnel VPN. monReseauNeutre monserveur.nohost.me sasha 